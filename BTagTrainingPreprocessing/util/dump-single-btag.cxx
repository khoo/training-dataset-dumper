#include <cstddef>
#include <memory>
#include <cmath>

#include "src/SingleBTagConfig.hh"

#include "SingleBTagTools.hh"
#include "SingleBTagOptions.hh"

#include "DecoratorExample.hh"
#include "TrackTruthDecorator.hh"
#include "TrackVertexDecorator.hh"

#include "TruthTools.hh"
#include "SafeShallowCopy.hh"

#include "TruthCorruptionCounter.hh"

#include "xAODRootAccess/Init.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include "PathResolver/PathResolver.h"

#include "H5Cpp.h"

#include "TFile.h"
#include "TTree.h"

namespace {

  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  constexpr float operator "" _GeV(unsigned long long d) { return d*1e3; }
  constexpr float operator "" _GeV(long double d) { return d*1e3; }
}

int main (int argc, char *argv[]) {
  using FlavorTagDiscriminants::DL2HighLevel;
  SingleTagIOOpts opts = get_single_tag_io_opts(argc, argv);
  const SingleBTagConfig jobcfg = get_singlebtag_config(opts.config_file_name);
  // The name of the application:
  const char *const APP_NAME = "BTagTestDumper";

  // Set up the environment:
  check_rc( xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  H5::H5File output(opts.out, H5F_ACC_TRUNC);
  SingleBTagTools tools(jobcfg, output);

  // this is just an example augmenter, it doesn't do anything important
  DecoratorExample example_decorator;
  TrackTruthDecorator trkTruthDecorator;
  TrackVertexDecorator trkVertexDecorator;

  TruthCorruptionCounter truth_counts;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {
    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    check_rc( event.readFrom(ifile.get()) );

    // Loop over its events:
    unsigned long long entries = event.getEntries();
    if (opts.max_events > 0) entries = std::min(opts.max_events, entries);
    for (unsigned long long entry = 0; entry < entries; ++entry) {

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }

      const xAOD::JetContainer *raw_jets = nullptr;
      check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );

      const xAOD::TruthParticleContainer *tpc = nullptr;
      check_rc( event.retrieve(tpc, "TruthParticles") );
      std::vector<const xAOD::TruthParticle*> truth_particles(
        tpc->begin(), tpc->end());

      const xAOD::EventInfo *event_info = nullptr;
      check_rc( event.retrieve(event_info, "EventInfo") );

      std::vector<const xAOD::TruthParticle*> truth_leptons;
      try {
        truth_leptons = truth::getLeptonsFromWZ(truth_particles);
        truth_counts.n_successful_truth_reads++;
      } catch (truth::TruthRecordError& err) {
        truth_counts.n_failed_truth_reads++;
        std::cerr << getEventInfoString(err, *event_info) << std::endl;
        continue;
      }

      const xAOD::VertexContainer* primary_vertices = nullptr;
      check_rc( event.retrieve(primary_vertices, jobcfg.vertex_collection));
      tools.dec.n_primary_vertices(*event_info) = primary_vertices->size();

      const xAOD::TruthEventContainer* truthEventContainer = nullptr;
      const xAOD::TruthVertex* truth_PV = nullptr;
      if ( jobcfg.decorate.track_truth_info ) {
        check_rc( event.retrieve(truthEventContainer, "TruthEvents"));
        truth_PV = truthEventContainer->at(0)->truthVertex(0); // truthEventContainer always has size == 1?
      }

      // first loop: add decorations to jets. These have to be done
      // before calibration to be consistent with reconstruction
      auto [jets, jetaux] = safeShallowCopyContainer(*raw_jets);
      for (const xAOD::Jet *const uncalib_jet : *jets) {

        // this is just applying example decorations, for no reason
        // other than to show that they work.
        example_decorator.decorate(*uncalib_jet);

        // this is more important stuff
        const xAOD::BTagging* btag = xAOD::BTaggingUtilities::getBTagging(
          *uncalib_jet);
        if (!btag) throw std::runtime_error("btagging object missing");
        if (jobcfg.decorate.jet_aug) {
          tools.jet_augmenter.augment(*btag, *btag);
        }
        if (jobcfg.decorate.soft_muon) {
          tools.muon_augmenter.augment(*btag);
        }
        if (jobcfg.decorate.btag_jes){
          tools.jet_augmenter.augmentBtagJes(*btag, *btag);
        }
        for (const auto& dl2: tools.dl2s) {
          dl2.decorate(*btag);
        }
      }
      if (jobcfg.do_calibration) {
        check_rc(tools.calibration_tool.applyCalibration(*jets));
      }
      // second loop: select calibrated jets and write out to HDF5
      std::vector<const xAOD::Jet*> jets_to_write;
      unsigned int rank = 0;
      for (size_t i = 0; i < jets->size(); i++) {
        const xAOD::Jet* calib_jet = jets->at(i);
        if (truth::is_overlaping_lepton(*calib_jet, truth_leptons, 0.3)) {
          continue;
        }

        tools.dec.jet_rank(*calib_jet) = rank++;
        if (jobcfg.do_calibration){
          // if we're calibrating jets we need to check the JVT again
          float updated_jvt_value= tools.jvttool.updateJvt(*calib_jet);
          tools.dec.jvt(*calib_jet) = updated_jvt_value;
          bool fail_jvt = (
            calib_jet->pt() > 20_GeV &&
            calib_jet->pt() < 60_GeV &&
            std::abs(calib_jet->eta()) < 2.4 &&
            updated_jvt_value < jobcfg.jvt_cut );
          if (fail_jvt) continue;

          if ( ! tools.cleaning_tool.keep(*calib_jet)) continue;

        }
        else {
          tools.dec.jvt(*calib_jet) = NAN;
        }

        if (calib_jet->pt() < jobcfg.pt_cut || std::abs(calib_jet->eta()) > 2.5) {
          continue;
        }

        jets_to_write.push_back(calib_jet);

        for (auto& tracktool: tools.tracks ) {
          const xAOD::Jet* uncalib_jet = raw_jets->at(i);
          auto tracks = tracktool.selector.get_tracks(*uncalib_jet);
          for (const auto& track: tracks) {
            tracktool.track_accessor.augment(*track, *uncalib_jet);
            if ( jobcfg.decorate.track_sv_info ) {
              trkVertexDecorator.decorate(*track, *uncalib_jet);
            }
          }
          if ( jobcfg.decorate.track_truth_info ) {
            trkTruthDecorator.decorateAll(tracks, truth_PV);
          }
          sort(tracks.begin(), tracks.end(), tracktool.sort);
          tracktool.writer.write(tracks, *uncalib_jet);
        }
      }
      tools.jet_writer.write(jets_to_write, event_info);
    }
  }

  writeTruthCorruptionCounts(truth_counts);
  Info( APP_NAME, "Successful run.");

  return 0;
}

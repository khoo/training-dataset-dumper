#ifndef BTAG_TRACK_WRITER_CONFIG_HH
#define BTAG_TRACK_WRITER_CONFIG_HH

#include <string>
#include <vector>

struct BTagTrackWriterConfig {
  std::string name;
  std::string ip_prefix;

  std::vector<std::size_t> output_size;

  std::vector<std::string> uchar_variables;
  std::vector<std::string> int_variables;
  std::vector<std::string> float_variables;
  std::vector<std::string> double_variables;
  // these variables come out of customGetter in FlavorTagDiscriminants
  std::vector<std::string> flavortagdiscriminants_sequences;
};


#endif

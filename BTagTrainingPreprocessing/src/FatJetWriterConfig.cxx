#include "FatJetWriterConfig.hh"

FatJetWriterConfig::FatJetWriterConfig():
  name(""),
  write_kinematics_relative_to_parent(false),
  write_substructure_moments(false)
{
}

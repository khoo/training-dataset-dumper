#ifndef TRACK_SELECTOR_CONFIG_HH
#define TRACK_SELECTOR_CONFIG_HH

#include <string>

enum class TrackSource {JET, BTAGGING};

struct TrackSelectorConfig
{
  float pt_minumum = 1e3;
  float d0_maximum = 1;
  float z0_maximum = 1.5;
  int si_hits_minimum = 7;
  int si_holes_maximum = 2;
  int pix_holes_maximum = 1;
  std::string ip_prefix = "btagIp_";
};

#endif

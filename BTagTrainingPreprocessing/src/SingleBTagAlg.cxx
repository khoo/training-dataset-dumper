#include "SingleBTagAlg.h"

#include "SingleBTagTools.hh"
#include "SafeShallowCopy.hh"
#include "TruthTools.hh"

#include "xAODBTagging/BTaggingUtilities.h"

#include "H5Cpp.h"

#include "AsgDataHandles/ReadHandle.h"

namespace {

  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  constexpr float operator "" _GeV(unsigned long long d) { return d*1e3; }
  constexpr float operator "" _GeV(long double d) { return d*1e3; }

  const xAOD::Vertex* primary(const xAOD::VertexContainer& vertices) {
    if (vertices.size() == 0) {
      throw std::runtime_error("no primary vertices");
    }
    for ( const xAOD::Vertex *vertex : vertices ) {
      if ( vertex->vertexType() == xAOD::VxType::PriVtx ) {
        return vertex;
      }
    }
    // if we find nothing else this should be the beam spot
    return vertices.front();
  }
}


SingleBTagAlg::SingleBTagAlg(const std::string& name,
                             ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator),
  m_output(nullptr),
  m_tools(nullptr)
{
  declareProperty("outputFile", m_output_file);
  declareProperty("configFileName", m_config_file_name);
  declareProperty("metadataFileName",
                  m_metadata_file_name = "userJobMetadata.json");
}

SingleBTagAlg::~SingleBTagAlg() {
}

  // these are the functions inherited from Algorithm
StatusCode SingleBTagAlg::initialize () {
  auto cfg = get_singlebtag_config(m_config_file_name);
  m_config.reset(new SingleBTagConfig(cfg));
  m_output.reset(new H5::H5File(m_output_file, H5F_ACC_TRUNC));
  m_tools.reset(new SingleBTagTools(*m_config, *m_output));

  m_jetKey = cfg.jet_collection;
  ATH_CHECK(m_jetKey.initialize());

  // some things are currently not implemented here
  if (cfg.decorate.track_truth_info || cfg.decorate.track_sv_info) {
    ATH_MSG_FATAL("We don't support track truth or track SV decorations");
    return StatusCode::FAILURE;
  }

  ATH_MSG_DEBUG("Initialized SingleBTagAlg");

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  const auto& jobcfg = *m_config;
  auto& tools = *m_tools;

  const xAOD::JetContainer *raw_jets = nullptr;
  check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );

  ATH_MSG_DEBUG("Processing " + std::to_string(raw_jets->size()) + " jets");

  // first loop: add decorations to jets. These have to be done
  // before calibration to be consistent with reconstruction
  auto [jets, jetaux] = safeShallowCopyContainer(*raw_jets);
  for (const xAOD::Jet* uncalib_jet: *jets) {

    // this is more important stuff
    const auto* btag = xAOD::BTaggingUtilities::getBTagging(*uncalib_jet);
    if (!btag) throw std::runtime_error("no btaggingLink");

    if (jobcfg.decorate.jet_aug) {
      tools.jet_augmenter.augment(*btag, *btag);
    }
    if (jobcfg.decorate.soft_muon) {
      tools.muon_augmenter.augment(*btag);
    }
    if (jobcfg.decorate.btag_jes){
      tools.jet_augmenter.augmentBtagJes(*btag, *btag);
    }
    for (const auto& dl2: tools.dl2s) {
      dl2.decorate(*btag);
    }
  }
  if (jobcfg.do_calibration) {
    check_rc(tools.calibration_tool.applyCalibration(*jets));
  }

  // sort jets by descending pt
  //
  // we make a new container first to preserve the indices
  std::vector<const xAOD::Jet*> sorted_jets(jets->begin(), jets->end());
  std::sort(sorted_jets.begin(), sorted_jets.end(),
            [](const auto* j1, const auto* j2) {
              return j1->pt() > j2->pt();
            });

  // second loop: select calibrated jets and write out to HDF5
  //
  // we'll need truth particles and event info
  //
  const xAOD::EventInfo *event_info = nullptr;
  check_rc( event.retrieve(event_info, "EventInfo") );
  std::vector<const xAOD::TruthParticle*> truth_leptons;
  try {
    const xAOD::TruthParticleContainer *tpc = nullptr;
    check_rc( event.retrieve(tpc, "TruthParticles") );
    std::vector<const xAOD::TruthParticle*> truth_particles(
      tpc->begin(), tpc->end());
    truth_leptons = truth::getLeptonsFromWZ(truth_particles);
    m_counts.n_successful_truth_reads++;
  } catch (truth::TruthRecordError& err) {
    ATH_MSG_ERROR(getEventInfoString(err, *event_info));
    m_counts.n_failed_truth_reads++;
    return StatusCode::SUCCESS;
  }

  // save some primary vertex information on eventinfo
  const xAOD::VertexContainer* primary_vertices = nullptr;
  check_rc( event.retrieve(primary_vertices, jobcfg.vertex_collection));
  tools.dec.n_primary_vertices(*event_info) = primary_vertices->size();
  tools.dec.primary_vertex_detector_z(*event_info) =
    primary(*primary_vertices)->z();

  std::vector<const xAOD::Jet*> jets_to_write;
  unsigned int rank = 0;
  for (const xAOD::Jet* calib_jet: sorted_jets) {
    if (truth::is_overlaping_lepton(*calib_jet, truth_leptons, 0.3)) {
      continue;
    }

    tools.dec.jet_rank(*calib_jet) = rank++;
    if (jobcfg.do_calibration){
      // if we're calibrating jets we need to check the JVT again
      float updated_jvt_value= tools.jvttool.updateJvt(*calib_jet);
      tools.dec.jvt(*calib_jet) = updated_jvt_value;
      bool fail_jvt = (
        calib_jet->pt() > 20_GeV &&
        calib_jet->pt() < 60_GeV &&
        std::abs(calib_jet->eta()) < 2.4 &&
        updated_jvt_value < jobcfg.jvt_cut );
      if (fail_jvt) continue;

      if ( ! tools.cleaning_tool.keep(*calib_jet)) continue;

    }
    else {
      tools.dec.jvt(*calib_jet) = NAN;
    }

    if (calib_jet->pt() < jobcfg.pt_cut || std::abs(calib_jet->eta()) > 2.5) {
      continue;
    }

    jets_to_write.push_back(calib_jet);

    for (auto& tracktool: tools.tracks ) {
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      auto tracks = tracktool.selector.get_tracks(*uncalib_jet);
      for (const auto& track: tracks) {
        tracktool.track_accessor.augment_with_ip(*track, *uncalib_jet);
      }
      sort(tracks.begin(), tracks.end(), tracktool.sort);
      tracktool.writer.write(tracks, *uncalib_jet);
    }
  }
  if (!jets_to_write.empty()) {
    tools.jet_writer.write(jets_to_write, event_info);
  }

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::finalize () {

  if (m_metadata_file_name.size() > 0) {
    writeTruthCorruptionCounts(m_counts, m_metadata_file_name);
  }

  return StatusCode::SUCCESS;
}

#include "TrackSelector.hh"
#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"


TrackSelector::TrackSelector(TrackSelectorConfig config,
                             const std::string& link_name,
                             TrackSource track_source):
  m_track_associator(link_name),
  m_track_select_cfg(config),
  m_track_source(track_source),
  m_acc(config.ip_prefix)
{}


TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  const SG::AuxElement* source(nullptr);
  if (m_track_source == TrackSource::BTAGGING) {
    source = xAOD::BTaggingUtilities::getBTagging(jet);
  } else if (m_track_source == TrackSource::JET) {
    source = &jet;
  } else {
    throw std::logic_error("configuration error: no track source");
  }
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto &link : m_track_associator(*source)) {
    if(link.isValid()) {
      const xAOD::TrackParticle *tp = *link;
      if (passed_cuts(*tp)) {
        tracks.push_back(tp);
      }
    } else {
      throw std::logic_error("invalid track link");
    }
  }
  return tracks;
}

bool TrackSelector::passed_cuts(const xAOD::TrackParticle& tp) const
{
  static SG::AuxElement::ConstAccessor<unsigned char> pix_hits("numberOfPixelHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_holes("numberOfPixelHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_shared("numberOfPixelSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_dead("numberOfPixelDeadSensors");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_hits("numberOfSCTHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_holes("numberOfSCTHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_shared("numberOfSCTSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_dead("numberOfSCTDeadSensors");


  if (std::abs(tp.eta()) > 2.5)
          return false;
  double n_module_shared = (pix_shared(tp) + sct_shared(tp) / 2);
  if (n_module_shared > 1)
    return false;
  if (tp.pt() <= m_track_select_cfg.pt_minumum)
    return false;
  if (std::isfinite(m_track_select_cfg.d0_maximum) &&
      std::abs(m_acc.d0(tp)) >= m_track_select_cfg.d0_maximum)
    return false;
  if (std::isfinite(m_track_select_cfg.z0_maximum) &&
      std::abs(m_acc.z0SinTheta(tp)) >= m_track_select_cfg.z0_maximum)
    return false;
  if (pix_hits(tp) + pix_dead(tp) + sct_hits(tp) + sct_dead(tp) < m_track_select_cfg.si_hits_minimum)
    return false;
  if ((pix_holes(tp) + sct_holes(tp)) > m_track_select_cfg.si_holes_maximum)
    return false;
  if (pix_holes(tp) > m_track_select_cfg.pix_holes_maximum)
    return false;
  return true;
}



#ifndef BTAGGING_WRITER_CONFIGURATION_HH
#define BTAGGING_WRITER_CONFIGURATION_HH

#include <vector>
#include <string>
#include <map>
#include <stdexcept>

namespace cfg {
  typedef std::vector<std::string> List;

  const std::map<std::string, std::vector<std::string> > BTagDefaultsMap = {
    {
      "IP2D_isDefaults",
      {
        "IP2D_pu", "IP2D_pc", "IP2D_pb",
        "IP2D_cu", "IP2D_bu", "IP2D_bc",
      }
    },
    {
      "IP3D_isDefaults",
      {
        "IP3D_pu", "IP3D_pc", "IP3D_pb",
        "IP3D_cu", "IP3D_bu", "IP3D_bc",
      }
    },
    {
      "rnnip_isDefaults",
      {
       "rnnip_pu", "rnnip_pc", "rnnip_pb", "rnnip_ptau",
      }
    },
    {
      "JetFitter_isDefaults",
      {
        "JetFitter_nVTX",
        "JetFitter_nSingleTracks",
        "JetFitter_nTracksAtVtx",
        "JetFitter_N2Tpair",
        "JetFitter_energyFraction",
        "JetFitter_mass",
        "JetFitter_significance3d",
        "JetFitter_deltaphi",
        "JetFitter_deltaeta",
        "JetFitter_massUncorr",
        "JetFitter_dRFlightDir",
        "JetFitter_deltaR",
      }
    },
    {
      "SV1_isDefaults",
      {
        "SV1_NGTinSvx",
        "SV1_N2Tpair",
        "SV1_masssvx",
        "SV1_efracsvx",
        "SV1_significance3d",
        "SV1_dstToMatLay",
        "SV1_deltaR",
        "SV1_Lxy",
        "SV1_L3d",
        "SV1_pu",
        "SV1_pc",
        "SV1_pb",
      }
    },
    {
      "JetFitterSecondaryVertex_isDefaults",
      {
        "JetFitterSecondaryVertex_mass",
        "JetFitterSecondaryVertex_energy",
        "JetFitterSecondaryVertex_energyFraction",
        "JetFitterSecondaryVertex_minimumTrackRelativeEta",
        "JetFitterSecondaryVertex_maximumTrackRelativeEta",
        "JetFitterSecondaryVertex_averageTrackRelativeEta",
        "JetFitterSecondaryVertex_displacement3d",
        "JetFitterSecondaryVertex_displacement2d",
        "JetFitterSecondaryVertex_nTracks",
        "JetFitterSecondaryVertex_maximumAllJetTrackRelativeEta",
        "JetFitterSecondaryVertex_minimumAllJetTrackRelativeEta",
        "JetFitterSecondaryVertex_averageAllJetTrackRelativeEta"
      }
    },
    {
      "SMT_isDefaults",
      {
        "SMT_discriminant",
        "SMT_mu_pt",
        "SMT_dR",
        "SMT_qOverPratio",
        "SMT_mombalsignif",
        "SMT_scatneighsignif",
        "SMT_pTrel",
        "SMT_mu_d0",
        "SMT_mu_z0",
        "SMT_ID_qOverP",
      }
    },
    {
      "softMuon_isDefaults",
      {
        "softMuon_pt",
        "softMuon_dR",
        "softMuon_eta",
        "softMuon_phi",
        "softMuon_qOverPratio",
        "softMuon_momentumBalanceSignificance",
        "softMuon_scatteringNeighbourSignificance",
        "softMuon_pTrel",
        "softMuon_ip3dD0",
        "softMuon_ip3dZ0",
        "softMuon_ip3dD0Significance",
        "softMuon_ip3dZ0Significance",
        "softMuon_ip3dD0Uncertainty",
        "softMuon_ip3dZ0Uncertainty",
      }
    },
  };

  // Tracks
  const List TrackUChars = {
    "numberOfInnermostPixelLayerHits",
    "numberOfNextToInnermostPixelLayerHits",
    "numberOfInnermostPixelLayerSharedHits",
    "numberOfInnermostPixelLayerSplitHits",
    "numberOfPixelHits",
    "numberOfPixelHoles",
    "numberOfPixelSharedHits",
    "numberOfPixelSplitHits",
    "numberOfSCTHits",
    "numberOfSCTHoles",
    "numberOfSCTSharedHits",
  };
  const List TrackInts = {
    "IP2D_grade",
    "IP3D_grade",
  };
  const List TrackFloats = {
    "chiSquared",
    "numberDoF",
    "IP3D_signed_d0",
    "IP2D_signed_d0",
    "IP3D_signed_z0",
    "IP3D_signed_d0_significance",
    "IP3D_signed_z0_significance",
    "btagIp_d0",
    "btagIp_z0SinTheta"
  };

  // utility
  inline void insert_into(std::vector<std::string>& into,
                          const List& inserted) {
    into.insert(into.end(), inserted.begin(), inserted.end());
  }
  inline std::map<std::string, std::string> check_map_from(
    const std::map<std::string, std::vector<std::string> >& default_to_vals){
    std::map<std::string, std::string> out;
    for (const auto& pair: default_to_vals) {
      const std::string& check_var = pair.first;
      for (const auto& var: pair.second) {
        if (out.count(var)) {
          throw std::logic_error(var + " already in map");
        }
        out[var] = check_var;
      }
    }
    return out;
  }
}

#endif

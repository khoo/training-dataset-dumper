#ifndef SINGLE_BTAG_TOOLS_HH
#define SINGLE_BTAG_TOOLS_HH

#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "TrackSelector.hh"
#include "trackSort.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

struct SingleBTagConfig;
struct TrackConfig;
namespace H5 {
  class H5File;
}

struct TrackTools {
  TrackTools(const TrackConfig&, H5::H5File&);
  TrackTools(TrackTools&&);
  TrackSelector selector;
  TrackSort sort;
  BTagTrackWriter writer;
  BTagTrackIpAccessor track_accessor;
};

struct SingleBTagTools {

  struct Decorators {
    SG::AuxElement::Decorator<float> jvt;
    SG::AuxElement::Decorator<int> jet_rank;
    SG::AuxElement::Decorator<int> n_primary_vertices;
    SG::AuxElement::Decorator<float> primary_vertex_detector_z;
    Decorators():
      jvt("bTagJVT"),
      jet_rank("jetPtRank"),
      n_primary_vertices("nPrimaryVertices"),
      primary_vertex_detector_z("primaryVertexDetectorZ")
      {}
  };

  SingleBTagTools(const SingleBTagConfig&, H5::H5File&);
  JetCalibrationTool calibration_tool;
  JetCleaningTool cleaning_tool;
  JetVertexTaggerTool jvttool;

  BTagJetAugmenter jet_augmenter;
  FlavorTagDiscriminants::BTagMuonAugmenter muon_augmenter;

  std::vector<FlavorTagDiscriminants::DL2HighLevel> dl2s;

  BTagJetWriter jet_writer;
  std::vector<TrackTools> tracks;

  Decorators dec;
};

#endif

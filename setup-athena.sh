# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
asetup Athena,22.0.40

# add h5ls
. ${BASH_SOURCE[0]%/*}/setup/add-h5-tools.sh
